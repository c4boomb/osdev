#ifndef _STDLIB_H
#define _STDLIB_H 1
 
#include <headers/cdefs.h>
#define INT_NUM_M 19
#ifdef __cplusplus
extern "C" {
#endif
 
__attribute__((__noreturn__))
void abort(void);


char* itoc(int i);
#ifdef __cplusplus
}
#endif
 
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <headers/stdio.h>
#include <headers/stdlib.h>

static uint16_t* const KEYBOARD = (uint16_t*) 0x60;
void idt_init();
void kb_init();
void keyboard_handler_main();

/* The following array is taken from 
    http://www.osdever.net/bkerndev/Docs/keyboard.htm
   All credits where due
*/


#include <headers/stdlib.h>
#include <headers/stdio.h>
 
__attribute__((__noreturn__))
void abort(void)
{
	// TODO: Add proper kernel panic.
	printf("Kernel Panic: abort()\n");
	while ( 1 ) { }
	__builtin_unreachable();
}

char* itoc(int i) {
	static char buffer[INT_NUM_M + 2];
	char* result = buffer + INT_NUM_M + 1;
	if(i >= 0) {
		do {
			*(--result) = '0' + (i%10);
			i /= 10;
		} while(i != 0);
		return result;
	} else {
		do {
			*(--result) = '0' - (i%10);
			i /= 10;
		} while(i!=0);
		*(--result) = '-';
	}
	return result;
}

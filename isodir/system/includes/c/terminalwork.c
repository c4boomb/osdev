#include <headers/terminalwork.h>
size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

static inline void outb(unsigned short port, unsigned char value) {
	asm volatile( "outb %0, %1" : : "a"(value), "Nd"(port) );
}

void terminal_initialize(void)
{
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_BLACK, COLOR_WHITE);
	terminal_buffer = VGA_MEMORY;
	for ( size_t y = 0; y < VGA_HEIGHT; y++ )
	{
		for ( size_t x = 0; x < VGA_WIDTH; x++ )
		{
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
	
}
 
void terminal_setcolor(uint8_t color)
{
	terminal_color = color;
}
 
void terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}
void move_up() {
	for( size_t y = 1; y < VGA_HEIGHT; y++ ) {
		for( size_t x = 0; x < VGA_WIDTH; x++ ) {
			const size_t index = (y-1) * VGA_WIDTH + x;
			const size_t index2 = y * VGA_WIDTH +x;
			if(y == VGA_HEIGHT-1)
				terminal_buffer[index2] = make_vgaentry(' ', terminal_color);
			terminal_buffer[index] = terminal_buffer[index2];
		}
	}
}
void terminal_putchar(char c)
{
	bool flagse = false;
	if(c == '\n') {
		terminal_row++;
		terminal_column = 0;
		flagse = true;
	}
	if(c == '\b') {
		if(terminal_column == 0) {
			terminal_row--;
			terminal_column = 80;
		}
		else {
			terminal_column--;
		}
		terminal_putentryat(' ', terminal_color, terminal_column, terminal_row);
		flagse = true;
	}
	if(c == '\t') {
		terminal_putentryat(' ', terminal_color, terminal_column++, terminal_row);
		if(terminal_column > 80) {
			terminal_row++;
			terminal_column = 0;
		}
		terminal_putentryat(' ', terminal_color, terminal_column++, terminal_row);
		if(terminal_column > 80) {
			terminal_row++;
			terminal_column = 0;
		}
		terminal_putentryat(' ', terminal_color, terminal_column++, terminal_row);
		if(terminal_column > 80) {
			terminal_row++;
			terminal_column = 0;
		}
		flagse = true;
	}
	if(!flagse) {
		terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
		if ( ++terminal_column == VGA_WIDTH )
		{
			terminal_column = 0;
			if ( ++terminal_row == VGA_HEIGHT )
			{
				move_up();
				terminal_row = VGA_HEIGHT-1;
			}
		}
	}
	
	unsigned char cur_loc = terminal_row*80+terminal_column;
	outb(0x3D4, 0x0F);
	outb(0x3D5, (unsigned char) (cur_loc));
	
	outb(0x3D4, 0x0E);
	outb(0x3D5, (unsigned char) (cur_loc>>8));
}
 
void terminal_write(const char* data, size_t size)
{
	for ( size_t i = 0; i < size; i++ )
		terminal_putchar(data[i]);
}
 
void terminal_writestring(const char* data)
{
	terminal_write(data, strlen(data));
}

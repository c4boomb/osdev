cd isodir/system
./build.sh
cd ../..
as -o objects/boot.o core/asm/boot.s --32
as -o objects/crti.o core/asm/crti.s --32
as -o objects/crtn.o core/asm/crtn.s --32
i686-elf-gcc -c core/c/kernel.c -o objects/kernel.o -I isodir/system/includes -std=gnu99 -D__is_myos_kernel -ffreestanding -O2 -Wextra -m32 -Xassembler --32
ld -T linker.ld -melf_i386 -o kernel.bin objects/crti.o objects/crtbegin.o isodir/system/objects/string.o isodir/system/objects/irq.o isodir/system/objects/terminalwork.o isodir/system/objects/stdio.o isodir/system/objects/stdlib.o objects/kernel.o objects/boot.o objects/crtend.o objects/crtn.o
cp kernel.bin isodir/boot/myos.bin
grub-mkrescue -o kernel.iso isodir

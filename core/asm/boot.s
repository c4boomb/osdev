.set ALIGN, 1<<0
.set MEMINFO, 1<<1
.set FLAGS, ALIGN | MEMINFO
.set MAGIC, 0x1BADB002
.set CHECKSUM, -(MAGIC + FLAGS)

.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

.section .bootstrap_stack, "aw", @no_bits
stack_bottom:
.skip 32768
stack_top:

.section .text
.global _start
.global keyboard_handler
.global read_port
.global write_port
.global load_idt

.type keyboad_handler, @function
.type read_port, @function
.type write_port, @function
.type load_idt, @function
.type _start, @function


read_port:
	mov 4(%esp), %edx 
	in %dx, %al
	ret
write_port:
	mov 4(%esp), %edx
	mov 4(%edx), %al 
	out %al, %dx
	ret

load_idt:
	mov 4(%esp), %edx
	lidt (%edx)
	
	ret
	
keyboard_handler:
	call keyboard_handler_main
	ret




_start:
	movl $stack_top, %esp
	call _init
	
	call kernel_main
	call keyboard_handler
	call _fini
	cli
	

.size _start, . - _start

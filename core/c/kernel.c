#if !defined(_cplusplus)
#include <stdbool.h>
#endif
#include <stddef.h>
#include <stdint.h>
#include <headers/terminalwork.h>
#include <headers/stdio.h>
#include <headers/irq.h>

#if defined(__linux__)
#error "Not cross-compiler"
#endif

#if !defined(__i386__)
#error "Not 32bit"
#endif

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif
extern unsigned char last_symbol;

void kernel_main()
{
	terminal_initialize();
	printf("                   GreatOS by Lev Grigoryev %d", 2);
	printf("\nInterpreter -> ");
	idt_init();
	kb_init();

	while(1) {
		if(last_symbol == 27)
			break;
		
		keyboard_handler_main();
		
	}
	printf("\n\n GoodBye!");
	
}
